import kotlin.math.absoluteValue

fun main() {
    val calculator = Calculator()
    val poland1 = calculator.calculate("1 2 +")
    val poland2 = calculator.calculate("1 2 + 3 4 - *")
    println(poland1)
    println(poland2)
}

/**
 * 逆ポーランド記法を計算する計算機クラス
 */
class Calculator {
    private val stackBox = Stack()

    fun calculate(formula: String) : Int {
        // 要素ごとに式を分解して格納したリスト
        val formulaList = formula.split(" ")

        /** @throws 不適切な長さの式 */
        require(formulaList.size in 3..199)  {"length of formula is out of range"}

        // 要素毎に処理を実行
        formulaList.forEach {
            if (it.toIntOrNull() == null) operatorCalculate(it)
            else integerPush(it.toInt())
        }

        /** @throws 計算結果のみがスタックに残る状態ではないとき */
        require(stackBox.stack.size == 1) {"Too many operands"}
        return stackBox.pop()
    }

    /**
     * 要素が演算子だったときの処理メソッド：stackの上から2つ取ってきて、計算値をstackにpush()する
     * @param 演算子
     */
    private fun operatorCalculate(operator: String) {
        when (operator) {
            "+" -> {
                stackBox.push(stackBox.pop() + stackBox.pop() )
            }
            "-" -> {
                val firstPop = stackBox.pop()
                stackBox.push(stackBox.pop() - firstPop )
            }
            "*" -> {
                val pop1 = stackBox.pop()
                val pop2 = stackBox.pop()

                /** @throws 計算途中の値がオーバーフローで */
                val pop2Limit = Math.pow(10.0,9.0) / pop1.absoluteValue
                require( pop2Limit > pop2.absoluteValue ) {"Calculated value is Overflowed."}

                stackBox.push(pop1 * pop2 )
            }
            /** @throws 不適格な文字列であった場合*/
            else -> {
                error("It's disallowed Symbol.")
            }
        }
    }

    /**
     * 要素が整数だったときの処理メソッド：そのままstackにpush()する
     */
    private fun integerPush(operand: Int) {
        /** @throws オペランドが指定範囲外 */
        require(operand < Math.pow(10.0, 6.0)) {"Too big operand"}

        stackBox.push(operand)
    }

}


/**
 * スタッククラス
 */

class Stack {

    /** stack用 のリスト*/
    val stack = mutableListOf<Int>()

    /**
     * stackから一番上の要素を削除して
     * その値を返す。
     */
    fun pop() : Int {
        /** @throws stackが空になる */
        require(!isEmpty()) {"stackBox is Empty"}
        return stack.removeAt(stack.lastIndex)
    }

    /**
     *stackに要素を追加するメソッド
     */
    fun push(element: Int) {
        stack.add(element)
    }

    /**
     * stackが空か 判定メソッド
     */
    private fun isEmpty() : Boolean {
        return stack.size == 0
    }
}